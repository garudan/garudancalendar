# README #

This is an extended calendar of http://www.w3schools.com/howto/howto_css_calendar.asp.
Only added js-functions to change calendar dynamically.

### What is this repository for? ###

* Quick and simple calendar implementation

### How do I get set up? ###

* import garudan.calendar.js, garudan.calendar.style.css
* import jquery-lib: example: ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js
* add html-element like example:

```
#!html

  <body onload="updateCalendar(CURRENT_YEAR, CURRENT_MONTH)">
        <div class="my-calendar">
            <!-- BO-garudan.calendar-section; Only to add this section; weekdays : (default: german) -->
            <div class="month">
            </div>
            <ul class="weekdays">
            <li>Mo</li>
            <li>Di</li>
            <li>Mi</li>
            <li>Do</li>
            <li>Fr</li>
            <li>Sa</li>
            <li>So</li>
            </ul>
            <ul class="days">
            </ul>
            <!-- EO-garudan.calendar-section -->
        </div>
  </body>
```


# Full html example

```
#!html

<!DOCTYPE html>

<html>
	<head>
		<!-- initial meta-data -->
		<meta name="author" content="apophisGarudan">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
                <!-- import garudan.calendar.sytles css -->
		<link rel="stylesheet" href="garudan.calendar.style.css">
		<!-- import jquery @version: 1.12.4 -->
	  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                <!-- import garudan.calendar.functions js -->
                <script src="garudan.calendar.js"></script>
                <!-- initial title -->
		<title>garudan.calendar sample</title>
	</head>
        <!-- onload call calendar with current date -->
	<body onload="updateCalendar(CURRENT_YEAR, CURRENT_MONTH)">
        <div class="my-calendar">
            <!-- BO-garudan.calendar-section; Only to add this section; weekdays : (default: german) -->
            <div class="month">
            </div>
            <ul class="weekdays">
            <li>Mo</li>
            <li>Di</li>
            <li>Mi</li>
            <li>Do</li>
            <li>Fr</li>
            <li>Sa</li>
            <li>So</li>
            </ul>
            <ul class="days">
            </ul>
            <!-- EO-garudan.calendar-section -->
        </div>
	</body>
</html>

```
# Demo
![garudan.calendar.png](https://bitbucket.org/repo/LA5g8K/images/2688823762-garudan.calendar.png)