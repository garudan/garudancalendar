
/**
 * CURRENT_DATE {Date} - current date
 * CURRENT_DAY {Number} - current day
 * CURRENT_MONTH {Number} - current month (0 - 11: 0 = january, 11 = december)
 * CURRENT_YEAR {Number} - current year
 */
var CURRENT_DATE = new Date();
var CURRENT_DAY = CURRENT_DATE.getDate();
var CURRENT_MONTH = CURRENT_DATE.getMonth();
var CURRENT_YEAR = CURRENT_DATE.getFullYear();
/**
 * TMP date variables to set up prev and next month
 */
var TMP_DAY = CURRENT_DAY;
var TMP_MONTH = CURRENT_MONTH;
var TMP_YEAR = CURRENT_YEAR;
/**
 * @function prevMonth
 *              navigates to previous month
 */
function prevMonth(){
    if(TMP_MONTH == 0){
        TMP_MONTH = 11;
        TMP_YEAR = TMP_YEAR - 1;
    }else{
        TMP_MONTH = TMP_MONTH - 1;
    }

    updateCalendar(TMP_YEAR, TMP_MONTH);

}
/**
 * @function nextMonth
 *              navigates to next month
 */
function nextMonth(){
    if(TMP_MONTH == 11){
        TMP_MONTH = 0;
        TMP_YEAR = TMP_YEAR + 1;
    }else{
        TMP_MONTH = TMP_MONTH + 1;
    }
    updateCalendar(TMP_YEAR, TMP_MONTH);
}
/**
 * @function updateCalendar
 *              updates calendar (default: current date)
 * year {Number} - change year
 * month {Number} - change month
 */
function updateCalendar(year, month){
    
    var monthName = resolveMonthName(month);
    var month_Div = $('<div class="month">');
    month_Div.append('<ul style="list-style-type:none"><li class="prev"><input type="button" onclick="prevMonth()" value="❮"></li><li class="next"><input type="button" onclick="nextMonth()" value="❯"></li><li style="text-align:center">'+monthName+'<br><span style="font-size:18px">'+year+'</span></li></ul>');
    $('.month').replaceWith(month_Div);

    updateDays(year, month);

}
/**
 * @function updateDays
 *              updtes current days to month
 * year {Number} - changed year
 * month {Number} - changed month
 */
function updateDays(year, month){
    var date = new Date(year, month, 1);
    var days = [];
    var firstDay;
    var daysUl = $('<ul class="days">');

    while (date.getMonth() == month) {
        days.push(new Date(date));
        date.setDate(date.getDate() + 1);
    }

     firstDay = days[0].getDay();

     if(firstDay == 0){
         daysUl.append('<li></li><li></li><li></li><li></li><li></li><li></li>');
     }else{
         for(var i = 1; i < firstDay; ++i){
             daysUl.append('<li></li>')
         }
     }

     for(var i = 0; i < days.length; ++i){
         var day = days[i];
         if(CURRENT_DAY == day.getDate() && CURRENT_MONTH == day.getMonth() && CURRENT_YEAR == day.getFullYear()){
            daysUl.append('<li><span class="active">'+day.getDate()+'</span></li>');
             
         }else{
            daysUl.append('<li>'+day.getDate()+'</li>');
         }
     }

     $('.days').replaceWith(daysUl);

}
/**
 * @function resolveMonthName
 *              resolves month-number to correct month-name (default: german)
 * month {Number} - month-number to get name 
 */
function resolveMonthName(month){
    switch(month){
        case 0: return 'Januar';
        case 1: return 'Februar';
        case 2: return 'Maerz';
        case 3: return 'April';
        case 4: return 'Mai';
        case 5: return 'Juni';
        case 6: return 'Juli';
        case 7: return 'August';
        case 8: return 'September';
        case 9: return 'Oktober';
        case 10: return 'November';
        case 11: return 'Dezember';

        default: return '';
    }
}




